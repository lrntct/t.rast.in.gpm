#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
MODULE:    t.rast.in.gpm

AUTHOR(S): Laurent Courty, Roberta K. M. Kurek

PURPOSE: Download and import IMERG data from GPM mission into GRASS GIS.

COPYRIGHT: (C) 2015-2021 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

#%module
#% description: Download, re-project, import and register data from GPM mission (IMERG products - variable: precipitation)
#% keywords: import
#% keywords: rainfall
#% keywords: precipitation
#%end

#%option
#% key: start_date
#% label: UTC start date of the data. Format yyyy-mm-dd.
#% required: yes
#% multiple: no
#%end

#%option
#% key: end_date
#% label: UTC end date of the data. Format yyyy-mm-dd.
#% required: yes
#% multiple: no
#%end

#%option G_OPT_STDS_OUTPUT
#% required: yes
#%end

#%option
#% key: product
#% type: string
#% label: Product to be downloaded
#% options: 3IMERGHHE, 3IMERGHHL, 3IMERGHH, 3IMERGDE, 3IMERGDL, 3IMERGDF, 3IMERGM
#% required: yes
#%end

#%option
#% key: version
#% type: string
#% label: Algorithm version
#% options: 06
#% required: yes
#%end

import sys
import os
import shutil
import tempfile
import requests
import subprocess
import math
from datetime import datetime, timedelta
import calendar

import grass.script as gscript
import grass.temporal as tgis
from grass.pygrass.gis.region import Region
from grass.pygrass.messages import Messenger
import grass.pygrass.utils as gutils

# server url
SERVER_URL = 'https://gpm1.gesdisc.eosdis.nasa.gov/opendap/hyrax/GPM_L3'

# EPSG code of GPM data projection
GPM_EPSG = '4326'  # lat/long WGS84

# max size of the GPM grid
GRID_MAXLON = 3599
GRID_MAXLAT = 1799
# convertion factor between lat/long coordinates and grid size
GRID_COEFF_LON = (GRID_MAXLON + 1) / 360
GRID_COEFF_LAT = (GRID_MAXLAT + 1) / 180


class IMERGFormatter(object):
    """Format URL for downloading level 3 IMERG GPM products.
    """
    # https://pmm.nasa.gov/sites/default/files/document_files/
    # FileNamingConventionForPrecipitationProductsForGPMMissionV1.4.pdf
    BASE_NAME = '3B-{dtype_b}{latency}.MS.MRG.3IMERG.{d}-S{s}-E{e}{seq}.{algo_ver}.{extension}.nc4'

    # product base names
    HALF_HOURLY = '3IMERGHH'
    DAILY = '3IMERGD'
    MONTHLY = '3IMERGM'
    # data type file-name field B
    DATA_TYPE_B = {DAILY: 'DAY',
                   HALF_HOURLY: 'HHR',
                   MONTHLY: 'MO'}

    # algorithm version code for each product
    ALGO_CODES = {'06': {'3IMERGDE': 'V06', '3IMERGDL': 'V06', '3IMERGDF': 'V06',
                         '3IMERGHHE': 'V06B', '3IMERGHHL': 'V06B', '3IMERGHH': 'V06B',
                         '3IMERGM': 'V06B'},
                  }
    # file extension
    FILE_EXTENSIONS = {DAILY: 'nc4',
                       HALF_HOURLY: 'HDF5',
                       MONTHLY: 'HDF5'}

    # IMERG data fields
    DATA_FIELD = {DAILY: 'precipitationCal',
                  HALF_HOURLY: 'precipitationCal',
                  MONTHLY: 'precipitation'}
    # Half-hourly and Daily:
    # precipitationCal = Multi-satellite precipitation estimate with gauge calibration (recommended for general use)
    # Monthly:
    # precipitation = Merged satellite-gauge precipitation estimate (recommended for general use)

    def __init__(self, product_name, grid_bounding_box, algo_ver):
        """
        """
        self.product = product_name
        self.bounding_box = grid_bounding_box
        self.algo_ver = algo_ver
        # end time of IMERG maps
        self.time_interv = self._get_time_interv()

    def get_full_dir(self, start_time):
        """return the full directory
        """
        assert isinstance(start_time, datetime)
        product_dir = self._get_product_dir()
        subdir = self._get_subdir(start_time)
        return '{}/{}/{}/'.format(SERVER_URL, product_dir, subdir)

    def get_file_name(self, start_time):
        """return a formatted file name according to a given start time.
        """
        # date
        str_day = start_time.strftime('%Y%m%d')
        # start and end times
        str_start = start_time.strftime('%H%M%S')
        str_end = (start_time + self.time_interv).strftime('%H%M%S')
        # sequence number
        str_seq = self._get_sequence(start_time)
        # data type file-name field B
        for product_basename in self.DATA_TYPE_B:
            if self.product.startswith(product_basename):
                dtype_b = self.DATA_TYPE_B[product_basename]
        # latency code
        latency = self._get_latency_code()
        # algorithm code
        algo_code = self.ALGO_CODES[self.algo_ver][self.product]
        # file extension
        for product_basename in self.FILE_EXTENSIONS:
            if self.product.startswith(product_basename):
                extension = self.FILE_EXTENSIONS[product_basename]
        return self.BASE_NAME.format(dtype_b=dtype_b, latency=latency,
                                     d=str_day, s=str_start, e=str_end,
                                     seq=str_seq, algo_ver=algo_code,
                                     extension=extension)

    def get_query_url(self):
        """return the complement URL defining the desired variable and extent.
        """
        query_list = []
        # variable
        for product_basename in self.DATA_FIELD:
            if self.product.startswith(product_basename):
                variable = self.DATA_FIELD[product_basename]
        var_query = '{var}[0:1:0][{min_lon}:{max_lon}][{min_lat}:{max_lat}]'.format(
                                var=variable,
                                min_lon=self.bounding_box['min_lon'],
                                max_lon=self.bounding_box['max_lon'],
                                min_lat=self.bounding_box['min_lat'],
                                max_lat=self.bounding_box['max_lat'])
        query_list.append(var_query)

        coor_query = 'lat[{min_lat}:{max_lat}],lon[{min_lon}:{max_lon}]'.format(
                                min_lon=self.bounding_box['min_lon'],
                                max_lon=self.bounding_box['max_lon'],
                                min_lat=self.bounding_box['min_lat'],
                                max_lat=self.bounding_box['max_lat'])

        query_list.append(coor_query)
        return '?' + ','.join(query_list)

    def get_time_step(self, start_time):
        """return adequate time step according to product type and current date
        """
        assert isinstance(start_time, datetime)
        if self.product.startswith(self.DAILY):
            return timedelta(days=1)
        elif self.product.startswith(self.HALF_HOURLY):
            return timedelta(minutes=30)
        elif self.product.startswith(self.MONTHLY):
            # number of days in the current  month depends on the current date
            num_days = calendar.monthrange(start_time.year, start_time.month)[1]
            return timedelta(days=num_days)
        else:
            assert False, u"{}: unknown product name".format(self.product)

    def _get_time_interv(self):
        """return timedelta to be added to map start date.
        """
        if self.product.startswith(self.DAILY) or self.product.startswith(self.MONTHLY):
            return timedelta(hours=23, minutes=59, seconds=59)
        elif self.product.startswith(self.HALF_HOURLY):
            return timedelta(minutes=29, seconds=59)
        else:
            assert False, u"{}: unknown product name".format(self.product)

    def _get_latency_code(self):
        """Get the latency code for file name.
        """
        # early
        if self.product.endswith('E'):
            return '-E'
        # late
        elif self.product.endswith('L'):
            return '-L'
        # final
        else:
            return ''

    def _get_product_dir(self):
        """return the corresponding product directory.
        """
        return 'GPM_{}.{}'.format(self.product, self.algo_ver)

    def _get_subdir(self, start_time):
        """For a given product name and datetime object,
        return the server subdirectories
        """
        year = str(start_time.year)
        if self.product.startswith(self.MONTHLY):
            return year
        elif self.product.startswith(self.DAILY):
            month = str(start_time.month)
            return '{}/{}'.format(year, month.zfill(2))
        elif self.product.startswith(self.HALF_HOURLY):
            # get the day of the year
            yday = str(start_time.timetuple().tm_yday)
            return '{}/{}'.format(year, yday.zfill(3))
        else:
            assert False, u"{}: unknow product name".format(self.product)

    def _get_sequence(self, start_time):
        """return the sequence field of file name
        """
        if self.product.startswith(self.HALF_HOURLY):
            # sequence is the number of minutes in the day
            str_min = str(start_time.hour * 60 + start_time.minute).zfill(4)
            return '.{}'.format(str_min)
        elif self.product.startswith(self.MONTHLY):
            # sequence is the month number
            str_month = str(start_time.month).zfill(2)
            return '.{}'.format(str_month)
        else:
            # for other data, 'sequence' is an empty field
            return ''


def main():
    # start messenger
    msgr = Messenger()
    try:
        start_date = str_to_datetime(options['start_date'])
    except ValueError:
        msgr.fatal(_(u"{}: format should be yyyy-mm-dd".format(options['start_date'])))
    try:
        end_date = str_to_datetime(options['end_date'])
    except ValueError:
        msgr.fatal(_(u"{}: format should be yyyy-mm-dd".format(options['end_date'])))

    # instantiate the formatter object
    grid_extent = get_grid_extent(get_llextent())
    product_obj = IMERGFormatter(options['product'], grid_extent, options['version'])
    query_url = product_obj.get_query_url()

    # create a temporary directory
    temp_dir = tempfile.mkdtemp()
    # create RasterDataset objects list
    raster_dts_lst = []
    # initiate temporal module
    tgis.init(raise_fatal_error=False)
    start_time = start_date
    # include the last day
    while start_time <= (end_date + timedelta(hours=23, minutes=59)):
        base_url = product_obj.get_full_dir(start_time)
        file_name = product_obj.get_file_name(start_time)

        full_url = base_url + file_name + query_url
        # communicate advance
        time_step = product_obj.get_time_step(start_time)
        end_time = start_time + time_step
        msgr.message("start: {}, end: {}".format(start_time, end_time))
        msgr.verbose(u"Getting {}".format(full_url))
        # download. Will take login information from .netrc file
        url_resp = requests.get(full_url)
        # catch 404
        if url_resp.status_code == 404:
            msgr.warning(u"No GPM file found for time {}".format(start_time))
            start_time += time_step
            continue
        elif url_resp.status_code != requests.codes.ok:
            msgr.fatal((u"{}: Can't download file."
                        " Error code: {}").format(start_time, url_resp.status_code))
        # write downloaded content to file
        with open(os.path.join(temp_dir, file_name), mode='wb') as local_file:
            local_file.write(url_resp.content)

        # Fix coordinates
        full_tempfile_path = os.path.join(temp_dir, file_name)
        try:
            netcdf_permute_coordinates(full_tempfile_path)
        except RuntimeError:
            msgr.fatal(u"<{}>: Not a NetCDF".format(full_tempfile_path))

        # set output file name
        start_str = datetime_to_str(start_time)
        end_str = datetime_to_str(end_time)
        output_file = "{}_S{}_E{}".format(options['output'], start_str, end_str)
        # reproject file
        reproj(os.path.join(temp_dir, file_name),
               os.path.join(temp_dir, output_file))
        # import file
        gscript.run_command('r.import', quiet=True, overwrite=gscript.overwrite(),
                            input=os.path.join(temp_dir, output_file), output=output_file)
        # change color table
        gscript.run_command('r.colors', quiet=True,
                            color='precipitation_daily', map=output_file)

        # create RasterDataset
        map_id = format_id(output_file)
        raster_dts = tgis.RasterDataset(map_id)
        # load spatial data from map
        raster_dts.load()
        # set time
        raster_dts.set_absolute_time(start_time=start_time,
                                     end_time=end_time)
        # populate the list
        raster_dts_lst.append(raster_dts)

        # update counters
        start_time += time_step

    # Register the maps in STRDS
    register_maps_in_strds(options['output'], raster_dts_lst)
    # delete the temporary directory
    shutil.rmtree(temp_dir)
    return 0


def datetime_to_str(datetime_obj):
    return datetime_obj.strftime('%Y%m%dT%H%M%S')


def str_to_datetime(date_str):
    return datetime.strptime(date_str, '%Y-%m-%d')


def netcdf_permute_coordinates(input_file):
    """permute coordinates from the given netcdf using ncpdq
    return the process return code
    """
    p = subprocess.Popen(['ncpdq', '-4', '-a', 'lat,lon', input_file, '-O', input_file],
                         stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    stdout, stderr = p.communicate()
    if b'ERROR' in stderr:
        raise RuntimeError


def get_llextent():
    """Return the region extent in lat/long
    """
    # check if location is lat/long or projected
    if gscript.locn_is_latlong():
        dict_extent = gscript.parse_command('g.region', flags='g')
        # keep only the needed values
        llextent = {k: float(v) for k, v in dict_extent.iteritems() if k in 'ewns'}
    else:
        # create and empty dict for results
        llextent = {i: None for i in 'ewns'}
        dict_extent = gscript.parse_command('g.region', flags='lg')
        # keep the maximum possible extent
        llextent['e'] = max(float(dict_extent['se_long']),
                            float(dict_extent['ne_long']))
        llextent['w'] = min(float(dict_extent['sw_long']),
                            float(dict_extent['nw_long']))
        llextent['n'] = max(float(dict_extent['ne_lat']),
                            float(dict_extent['nw_lat']))
        llextent['s'] = min(float(dict_extent['se_lat']),
                            float(dict_extent['sw_lat']))
    return llextent


def get_grid_extent(llextent):
    """From a lat/long bounding box, return a grid bounding box (# of cells)
    """
    # round one degree up or down (better fetch a bit more), change to positive values
    min_lon_cor = math.floor(llextent['w']) + 180
    max_lon_cor = math.ceil(llextent['e']) + 180
    min_lat_cor = math.floor(llextent['s']) + 90
    max_lat_cor = math.ceil(llextent['n']) + 90

    grid_extent = {'min_lon': None, 'max_lon': None,
                   'min_lat': None, 'max_lat': None}
    grid_extent['min_lon'] = int(max(0, min_lon_cor * GRID_COEFF_LON))
    grid_extent['min_lat'] = int(max(0, min_lat_cor * GRID_COEFF_LAT))
    grid_extent['max_lon'] = int(min(GRID_MAXLON, max_lon_cor * GRID_COEFF_LON))
    grid_extent['max_lat'] = int(min(GRID_MAXLAT, max_lat_cor * GRID_COEFF_LAT))
    return grid_extent


def reproj(input_file, output_file):
    '''Reproject input file from WGS84 to grass projection
    '''
    target_srs = gscript.read_command('g.proj', flags='jf')
    subprocess.call(['gdalwarp', '-q', '-s_srs', 'EPSG:{}'.format(GPM_EPSG),
                     '-t_srs', target_srs, input_file, output_file])


def format_id(name):
    '''Take a map or stds name as input
    and return a fully qualified name, i.e. including mapset
    '''
    mapset = gutils.getenv('MAPSET')
    if '@' in name:
        return name
    else:
        return '@'.join((name, mapset))


def register_maps_in_strds(strds_name, raster_dts_lst):
    '''Create STRDS and register maps from list
    '''
    # create strds
    strds_id = format_id(strds_name)
    strds_title = "data from GPM"
    strds_desc = ""
    strds = tgis.open_new_stds(strds_id, 'strds', 'absolute',
                               strds_title, strds_desc, "mean",
                               overwrite=gscript.overwrite())

    # Register the maps
    tgis.register.register_map_object_list('raster', raster_dts_lst,
                                           strds, delete_empty=True, unit=None)


if __name__ == "__main__":
    options, flags = gscript.parser()
    sys.exit(main())
